//
//  ViewController.swift
//  SkipViewController
//
//  Created by See.Ku on 2016/06/01.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	/// 次の次の画面に一気に遷移
	@IBAction func onSkipViewController(sender: AnyObject) {

		// 次の画面を作成
		let vc2 = storyboard?.instantiateViewControllerWithIdentifier("ViewController2")

		// 次の次の画面を作成
		let vc3 = storyboard?.instantiateViewControllerWithIdentifier("ViewController3")

		// 遷移後のViewControllerの配列を作成
		let ar = [self, vc2!, vc3!]

		// まとめて設定
		navigationController?.setViewControllers(ar, animated: true)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

