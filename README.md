
# 概要 ###########################################

## 概要

UINavigationControllerを使って、次の次の画面に一気に遷移する方法のまとめです。


## 動作環境

|環境		|情報			|
|-------	|---------------|
|Xcode		|7.3.1 (7D1014)	|
|iOS		|9.0			|
|Swift		|2.2			|
|Date		|2016/6/1		|


## 詳細

1. Storyboardで各ViewControllerにStoryboard IDを設定して、ViewControllerを生成できるようにしておきます
2. 途中の画面・遷移後の画面のインスタンスを生成します
3. 遷移元の画面を含める形で、遷移後のViewControllerの配列を生成します
4. setViewControllers()で遷移します

ソースコードはこんな感じになります。

```swift
	/// 次の次の画面に一気に遷移
	@IBAction func onSkipViewController(sender: AnyObject) {

		// 次の画面を作成
		let vc2 = storyboard?.instantiateViewControllerWithIdentifier("ViewController2")

		// 次の次の画面を作成
		let vc3 = storyboard?.instantiateViewControllerWithIdentifier("ViewController3")

		// 遷移後のViewControllerの配列を作成
		let ar = [self, vc2!, vc3!]

		// まとめて設定
		navigationController?.setViewControllers(ar, animated: true)
	}
```

簡単ですね。


## 連絡先

* Twitter: [@See.Ku](https://twitter.com/See_Ku)
* Website: [開発メモ](http://seeku.hateblo.jp/)


## ライセンス

ソースコードのライセンスは CC0 とします。

Creative Commons — CC0 1.0 Universal  
http://creativecommons.org/publicdomain/zero/1.0/

